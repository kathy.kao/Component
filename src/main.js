import Vue from 'vue'
import Footer from './components/footer'
import i18n from './lang'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#footer188',
  i18n,
  components: { Footer },
  template: "<Footer :token='token' :lan='lan' :domain='domain' :prod='prod'/>",
  created: function() {
    this.token = document.getElementById('footer188').getAttribute('token')
    this.lan = document.getElementById('footer188').getAttribute('lan')
    this.domain = document.getElementById('footer188').getAttribute('domain')
    this.prod = document.getElementById('footer188').getAttribute('prod')

    //set language
    i18n.locale = this.lan

    console.log('Main.js => token: ' + this.token)
    console.log('Main.js => lan: ' + this.lan)
    console.log('Main.js => domain: ' + this.domain)
    console.log('Main.js => prod: ' + this.prod)
  }
})
