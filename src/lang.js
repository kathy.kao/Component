import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from './resource/en-gb.json' // 存放英文翻譯
import cn from './resource/zh-cn.json' // 存放中文翻譯

Vue.use(VueI18n)

const messages = {
  'en-gb': en,
  'zh-cn': cn
}

const i18n = new VueI18n({
  locale: 'en-gb', //default language
  messages
})

export default i18n
