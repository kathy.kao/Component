function toLowerCase(input) {
  return input.toLowerCase()
}

function attrsFilter(input, prop, condition, reverse) {
  reverse = reverse || false
  var out = []
  input.forEach(function(obj) {
    if (typeof obj[prop] === 'undefined') return
    var allows = !Array.isArray(obj[prop])
      ? obj[prop].split(',').map(function(item) {
          return item
        })
      : obj[prop]
    var result = allows.indexOf('*') !== -1 || allows.indexOf(condition) !== -1
    if ((reverse && result) || (!reverse && !result)) out.push(obj)
  })
  return out
}

export { toLowerCase, attrsFilter }
