const socialMedia = {
  youtube: [
    { allow: 'Brazil', account: '188BETBRASIL' },
    { allow: 'UK,ROE', account: '188BET' },
    { allow: 'Vietnam', account: 'c/188BETVietNamofficial' },
    { allow: 'default', account: '188BETASIA' }
  ],
  twitter: [
    { allow: 'Brazil', account: '188betbrasil' },
    { allow: 'Indonesia', account: '188BETINDONESIA' },
    { allow: 'default', account: '188BET' }
  ],
  facebook: [
    { allow: 'Korea,Malaysia,ROA,ROW', account: 'ASIA188BET' },
    { allow: 'Brazil', account: '188BETBRASIL' },
    { allow: 'UK,ROE', account: '188BETUK' },
    { allow: 'Japan', account: '188BETJapan' },
    { allow: 'Cambodia', account: '188BETCAMBODIA' },
    { allow: 'Indonesia', account: 'groups/683781788490315/' },
    { allow: 'Thailand', account: '188BETThailandOfficial' },
    { allow: 'Vietnam', account: 'VN188BET' },
    { allow: 'default', account: '188BET' }
  ]
}

export { socialMedia }
