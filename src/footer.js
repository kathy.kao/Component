import { toLowerCase, attrsFilter } from './filter'
import { socialMedia } from './const'
import axios from 'axios-jsonp-pro'
export default {
  name: 'footer',
  props: ['token', 'lan', 'domain', 'prod'],
  data() {
    return {
      urlHost: this.domain,
      gv: [],
      uv: { login: this.token !== 'false' }, //Need to check wiht token Decrypt
      currRegion: [{ display: '' }],
      conditEmails: [{ account: '' }],
      cooperativeSet: {
        sponsor: [],
        paymentMethod: [],
        responsibleGaming: [],
        socialMedia: [],
        issueLicenses: []
      },
      socialMedia: socialMedia
    }
  },
  created: function() {
    this.fetchData()
  },
  methods: {
    fetchData: function() {
      var self = this
      const promise = axios.jsonp(
        'http://' +
          self.domain +
          '/service/thirdpartyapi/getVariablesFromToken?token=' +
          self.token +
          '&prod=' +
          self.prod
      )
      promise
        .then(function(response) {
          console.log('getVariablesFromToken Data Sucess:' + response)
          self.gv = JSON.parse(response)

          //get current region, self.gv.r => regionCode
          self.currRegion = self.gv.regs.filter(reg => reg.code === self.gv.r)

          //get CS email by region & lang
          var csEmails = self.gv.emails.filter(
            item => item.accountCategory === 'CS'
          )

          if (csEmails.length === 1) {
            self.conditEmails =
              csEmails[0].group.filter(
                item =>
                  item.supportRegs.indexOf(self.gv.r) !== -1 &&
                  item.supportLans.indexOf(self.lan) !== -1
              ) || []
          }

          //get cooperativeSet sponsor by region
          self.cooperativeSet.sponsor = attrsFilter(
            self.gv.cooperativeSet.sponsor.partners,
            'allows',
            self.gv.r,
            true
          )

          //get cooperativeSet paymentMethod by region
          self.cooperativeSet.paymentMethod = attrsFilter(
            self.gv.cooperativeSet.paymentMethod.partners,
            'allows',
            self.gv.r,
            true
          )

          //get cooperativeSet securityTrust by product & region
          var securityTrust = attrsFilter(
            self.gv.cooperativeSet.securityTrust.licenses,
            'prods',
            //'esports',
            self.prod,
            true
          )
          self.cooperativeSet.securityTrust = attrsFilter(
            securityTrust,
            'allows',
            self.gv.r,
            true
          )

          //get cooperativeSet responsibleGaming by product & region
          var responsibleGaming = attrsFilter(
            self.gv.cooperativeSet.responsibleGaming.licenses,
            'prods',
            //'esports',
            self.prod,
            true
          )
          self.cooperativeSet.responsibleGaming = attrsFilter(
            responsibleGaming,
            'allows',
            self.gv.r,
            true
          )

          //get cooperativeSet issueLicenses by product & region
          var issueLicenses = attrsFilter(
            self.gv.cooperativeSet.issueLicenses.licenses,
            'prods',
            self.prod,
            true
          )
          self.cooperativeSet.issueLicenses = attrsFilter(
            issueLicenses,
            'allows',
            self.gv.r,
            true
          )

          //social media logic
          self.cooperativeSet.socialMedia = attrsFilter(
            self.gv.cooperativeSet.socialMedia.partners,
            'allows',
            self.gv.r,
            true
          )
          self.cooperativeSet.socialMedia.forEach(function(item) {
            if (typeof self.socialMedia[item.name] !== 'undefined') {
              var matchItem = self.socialMedia[item.name].filter(
                partner =>
                  partner.allow.indexOf(self.gv.r) !== -1 ||
                  partner.allow === 'default'
              )
              if (self.socialMedia[item.name] && matchItem.length > 0) {
                item.partnerUrl = item.partnerUrl + matchItem[0].account
              }
            }
          })
        })
        .catch(function(error) {
          console.log('getVariablesFromToken Error:' + error)
        })
    }
  },
  filters: {
    toLowerCase
  }
}
